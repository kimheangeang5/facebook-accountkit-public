//
//  MainViewController.swift
//  Facebook_AccountKit
//
//  Created by Kimheang on 12/1/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import AccountKit
class MainViewController: UIViewController {

    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    var accountKit: AKFAccountKit!
    override func viewDidLoad() {
        super.viewDidLoad()
        if accountKit == nil {
            self.accountKit = AKFAccountKit(responseType: .accessToken)
            accountKit.requestAccount { (account, error) in
                if let id = account?.accountID {
                    self.idLabel.text = id
                }
                if let phone = account?.phoneNumber{
                    self.phoneNumberLabel.text = phone.stringRepresentation()
                }
                else if let email = account?.emailAddress{
                    self.phoneNumberLabel.text = email
                }
            }
        }
    }
    
    @IBAction func logoutTap(_ sender: Any) {
        accountKit.logOut()
        dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

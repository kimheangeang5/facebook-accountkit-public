//
//  ViewController.swift
//  Facebook_AccountKit
//
//  Created by Kimheang on 12/1/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import AccountKit

class ViewController: UIViewController {
    
    var accountKit: AKFAccountKit!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Create or init Account Kit
        if accountKit == nil {
            accountKit = AKFAccountKit(responseType: .accessToken)
        }
        
    }
    
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        //Customize Theme
//        let theme: AKFTheme = AKFTheme.default()
//        theme.headerBackgroundColor = .green
//        theme.headerTextColor = .red
//        theme.iconColor = .red
//        theme.inputTextColor = .brown
//        theme.textColor = .darkGray
//        theme.statusBarStyle = .lightContent
//        theme.titleColor = .yellow
//        loginViewController.setTheme(theme)
        
    }
    
    
    
    func loginWithPhone(){
        let inputState = UUID().uuidString
        let vc = (accountKit?.viewControllerForPhoneLogin(with: nil, state: inputState))!
        vc.enableSendToFacebook = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc, animated: true, completion: nil)
    }
    func loginWithEmail() {
        let inputState = UUID().uuidString
        let vc = (accountKit?.viewControllerForEmailLogin(withEmail: nil, state: inputState))!
        vc.enableSendToFacebook = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func loginViaEmailTap(_ sender: Any) {
        loginWithEmail()
    }
    
    @IBAction func loginViaPhoneTap(_ sender: Any) {
        loginWithPhone()
    }
}
extension ViewController : AKFViewControllerDelegate{
    //handle some login event
    func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AKFAccessToken!, state: String!) {
        print("did complete login with access token \(accessToken.tokenString) state \(state)")
    }
    
    // handle callback on successful login to show authorization code
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("didCompleteLoginWithAuthorizationCode")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        // ... handle user cancellation of the login process ...
        print("viewControllerDidCancel")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        // ... implement appropriate error handling ...
        print("\(viewController) did fail with error: \(error.localizedDescription)")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        if accountKit?.currentAccessToken != nil{
            // if the user is already logged in, go to the main screen
            print("Already Logged in")
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "showDetail", sender: self)
            })
        }
    }
    
    
    
}
